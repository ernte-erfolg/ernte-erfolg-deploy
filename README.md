# Ernte Erfolg Deploy

Make sure you have docker and docker-compose installed.

```sh
which docker
which docker-compose
```

Otherwise install it

- https://docs.docker.com/install/linux/docker-ce/debian/
- https://docs.docker.com/compose/install/

## Deployment

Make sure the configuration in the `.env` is correct.

Then use the deployment script: `./deployer push|pull|build|deploy|log|exec development|production` - see [the script](./deployer) for more.

# Run a specific git commit hash

`./deployer up production|development|local <Frontend commit hash> <Backend commit hash>`

To specify only the backend commit hash, replace the frontend commit hash placeholder with "" (and vice versa).


# Run locally

* Inside the frontend directory, run: `docker build -t ernte-erfolg-frontend:local .`
* Inside the backend directory, run: `docker build -t ernte-erfolg-service:local .`
* Inside the deploy directory, run: `./deployer up local`

# Kubernetes Notes
* Create Storage Class: https://docs.aws.amazon.com/de_de/eks/latest/userguide/ebs-csi.html
* Cert-Manager: https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes
