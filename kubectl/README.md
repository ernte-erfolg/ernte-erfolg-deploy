this dockerfile helps to get a console with kubectl

    docker build --tag test .
    docker run -it --rm test bash

or use docker-compose - this will store the credentials in ./config

    docker-compose build
    docker-compose run --rm kubectl-aws-client

configure inside the docker to hook up config with ernteerfolg cluster

    aws configure    
    aws eks --region eu-central-1 update-kubeconfig --name ernteerfolg
    
important: this rights have to be defined before any user can access the cluster via kubectl

    eksctl create iamidentitymapping --cluster ernteerfolg --arn arn:aws:iam::671411505887:user/ulicodecare --group system:masters --username ops-user
