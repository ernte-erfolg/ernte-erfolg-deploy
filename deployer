#!/bin/bash
#
# This is a simple deploy helper script
#
# Allowed arguments are:
# 
# - `push`: Push images
# - `pull`: Push images
# - `build`: Build all images in docker-compose.yml
# - `up`: Start via docker-compose.yml
# - `down`: Stop via docker-compose.yml
# - `log`: View logs of frontend container
# - `exec`: Exec bash in frontend container
#
# Usage `./deployer build && ./deployer push`

set -e

ACTION="$1"
export BUILD_ENV="${2:-local}"
FRONTEND_COMMIT_HASH="$3"
BACKEND_COMMIT_HASH="$4"
COMMAND=sh

if [[ ! -f ".env.$BUILD_ENV" ]]; then
  echo ".env.$BUILD_ENV does not exist, run 'cp .env.sample .env.$BUILD_ENV' to start with the default" && exit 1
fi

# https://gist.github.com/judy2k/7656bfe3b322d669ef75364a46327836
# Export the vars in .env.$BUILD_ENV into your shell:
export $(egrep -v '^#' ".env.$BUILD_ENV" | xargs)

if [[ "$FRONTEND_COMMIT_HASH" != '' ]]; then
  export FRONTEND_IMAGE="${DOCKER_REGISTRY}/${FRONTEND_IMAGE_NAME}:${FRONTEND_COMMIT_HASH}"
elif [[ "$BUILD_ENV" == 'local' ]]; then
  export FRONTEND_IMAGE="${FRONTEND_IMAGE_NAME}:${FRONTEND_BUILD}"
else
  export FRONTEND_IMAGE="${DOCKER_REGISTRY}/${FRONTEND_IMAGE_NAME}:${FRONTEND_BUILD}"
fi

if [[ "$BACKEND_COMMIT_HASH" != '' ]]; then
  export BACKEND_IMAGE="${DOCKER_REGISTRY}/${BACKEND_IMAGE_NAME}:${BACKEND_COMMIT_HASH}"
elif [[ "$BUILD_ENV" == 'local' ]]; then
  export BACKEND_IMAGE="${BACKEND_IMAGE_NAME}:${BACKEND_BUILD}"
else
  export BACKEND_IMAGE="${DOCKER_REGISTRY}/${BACKEND_IMAGE_NAME}:${BACKEND_BUILD}"
fi

echo "Will $ACTION for $BUILD_ENV"

if [[ "$ACTION" == 'build' ]]; then
  echo "Building all images"
  docker-compose -f docker-compose.yml -f "docker-compose.${BUILD_ENV}.yml" build
fi

if [[ "$ACTION" == 'push' ]]; then
  echo "Pushing image ${DOCKER_REGISTRY}/${FRONTEND_IMAGE_NAME}:${FRONTEND_BUILD}"
  docker push "${DOCKER_REGISTRY}/${FRONTEND_IMAGE_NAME}:${FRONTEND_BUILD}"
  echo "Pushing image ${DOCKER_REGISTRY}/${BACKEND_IMAGE_NAME}:${BACKEND_BUILD}"
  docker push "${DOCKER_REGISTRY}/${BACKEND_IMAGE_NAME}:${BACKEND_BUILD}"
fi

if [[ "$ACTION" == 'pull' ]]; then
  echo "Pulling image ${DOCKER_REGISTRY}/${FRONTEND_IMAGE_NAME}:${FRONTEND_BUILD}"
  docker pull "${DOCKER_REGISTRY}/${FRONTEND_IMAGE_NAME}:${FRONTEND_BUILD}"
  echo "Pulling image ${DOCKER_REGISTRY}/${BACKEND_IMAGE_NAME}:${BACKEND_BUILD}"
  docker pull "${DOCKER_REGISTRY}/${BACKEND_IMAGE_NAME}:${BACKEND_BUILD}"
fi

if [[ "$ACTION" == 'up' ]]; then
  echo "Starting all containers"
  echo "Frontend image: $FRONTEND_IMAGE"
  echo "Backend image: $BACKEND_IMAGE"
  if [[ "$BUILD_ENV" != 'local' ]]; then
    docker pull "$FRONTEND_IMAGE"
    docker pull "$BACKEND_IMAGE"
  fi
  docker-compose -p "ee_${BUILD_ENV}" -f docker-compose.yml -f "docker-compose.${BUILD_ENV}.yml" up -d
fi

if [[ "$ACTION" == 'down' ]]; then
  echo "Stopping all containers"
  docker-compose -p "ee_${BUILD_ENV}" -f docker-compose.yml -f "docker-compose.${BUILD_ENV}.yml" down
fi

if [[ "$ACTION" == 'log' ]]; then
  SERVICE="${FRONTEND_COMMIT_HASH:-frontend}"
  echo "View logs of $SERVICE"
  docker-compose -p "ee_${BUILD_ENV}" -f docker-compose.yml -f "docker-compose.$BUILD_ENV.yml" logs -f $SERVICE
fi

if [[ "$ACTION" == 'exec' ]]; then
  echo "Exec into $SERVICE"
  docker-compose -p "ee_${BUILD_ENV}" -f docker-compose.yml -f "docker-compose.$BUILD_ENV.yml" exec $SERVICE $COMMAND
fi
