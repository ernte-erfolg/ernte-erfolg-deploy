# change password procedure

beware of probes and restart loops!
database is running in kubernetes
deployed via helm
using persistent volume claim


config

    POSTGRES_DB=ernte_erfolg
    POSTGRES_PASSWORD=<from secrets: postgresql-credentials:postgresql-password>                                               
    POSTGRES_USER=postgres

        
cheat sheet

    kubectl -nee-dev get pod ee-dev-backend-postgresql-0 -o yaml
    kubectl -nee-dev get secret postgresql-credentials -o yaml    
    kubectl -nee-dev exec -it ee-dev-backend-postgresql-0 bash
    psql ernte_erfolg postgres

    
dependencies

    backend and database rely on the same kubernetes secret
    adminer does not store password
    

# steps

####01 change password of running postgresql container using 
    
    
    kubectl -nee-dev exec -it ee-dev-backend-postgresql-0 bash
    psql ernte_erfolg postgres
    ALTER USER postgres WITH PASSWORD '***';
    
this will result in backend not being able to get new connections
    
####02 set new password in gitlab variables

    https://gitlab.com/ernte-erfolg/ernte-erfolg-service/-/settings/ci_cd
    
####03 trigger new deploy 

    https://gitlab.com/ernte-erfolg/ernte-erfolg-service/pipelines 
    